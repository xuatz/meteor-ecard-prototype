ECardController = RouteController.extend({

})

Router.route('/', {
    action:function(){
        this.render('ecardpage');
    },
    data:function () {
        var result;
        result = {
            sendToName:'GENERIC NAMA',
            sendToEmail:'whatis@thisfor.com',
            msg:'this is a generic message, and i can\'t think of any thing to say so imma lorem ipsum some shit. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            createdBy:'system'
        }
        return result;
    }
})

Router.route(
    '/:cardID', {
        data: function() {
            var result = ECardDetails.findOne({
                _id: this.params.cardID
            });
            var result2 = {
                sendToName: this.params.cardID
            }
            return result;
        },
        name: 'ECardPage',
        controller: 'ECardController',
        action: function() {

            if (!this.data()) {
                Router.go('/');
            } else {
                this.render('ecardpage');
            }
        },
        waitOn: function() {
            return Meteor.subscribe('getCard', this.params.cardID);
        },
        loadingTemplate: 'ecardloading'
    }
);
