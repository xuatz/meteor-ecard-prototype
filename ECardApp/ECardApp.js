ECardDetails = new Mongo.Collection('ecard_details');

var beforeInsertOptions = function(username, doc) {
	doc.createdDttm = moment().format();
};

ECardDetails.before.insert(beforeInsertOptions);

Schemas = {};

Schemas.ECardDetails = new SimpleSchema({
    sendToEmail: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: "E-mail address"
    },
    sendToName: {
        type: String,
        label: "Name",
        optional: true
    },
    msg: {
        type: String,
        label: "Message for your friends",
        optional: true
    },
    createdBy: {
        type: String,
        label: "Your Name",
        optional: true
    },
    createdAt: {
      type: String,
        label: "Your Name",
        optional: true 
    }
});

AdminConfig = { 
    adminEmails: ['moltencrap@gmail.com'],
    collections: { 
        ECardDetails: {}
    } 
}

//===========================

if (Meteor.isClient) {

}

if (Meteor.isServer) {
    Meteor.publish('getCard', function(cardID) {
        var result;
        result = ECardDetails.find({
            _id: cardID
        });
        return result;
    });
}
